/* Dooba SDK
 * Nomad LiPo Battery Module Driver
 */

#ifndef	__NOMAD_H
#define	__NOMAD_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Voltage Reference
#define	NOMAD_VREF								3300UL

// Voltage Calibration
#define	NOMAD_VMAX								4500UL
#define	NOMAD_V100								4200UL
#define	NOMAD_V0								3400UL
#define	NOMAD_VCAL								((NOMAD_VMAX * 100000) / (NOMAD_VREF * 10))

// Voltage Margin
#define	NOMAD_VMARGIN							50UL

// Reference Values
#define	NOMAD_NV100								(NOMAD_V100 - NOMAD_VMARGIN)
#define	NOMAD_NV0								(NOMAD_V0 + NOMAD_VMARGIN)

// Nomad Structure
struct nomad
{
	// Battery Level Pin (Analog Input)
	uint8_t bat_level_pin;

	// Charging Status Pin
	uint8_t charging_pin;
};

// Initialize Nomad
extern void nomad_init(struct nomad *n, uint8_t bat_level_pin, uint8_t charging_pin);

// Pre-Initialize Nomad
extern void nomad_preinit(uint8_t bat_level_pin, uint8_t charging_pin);

// Is Charging
extern uint8_t nomad_is_charging(struct nomad *n);

// Get Raw Battery Level
extern uint8_t nomad_get_bat_level(struct nomad *n);

// Get Battery Voltage (millivolts)
extern uint16_t nomad_get_vbat(struct nomad *n);

// Get Battery Percentage
extern uint8_t nomad_get_bat_pct(struct nomad *n);

#endif
