/* Dooba SDK
 * Nomad LiPo Battery Module Driver
 */

// External Includes
#include <string.h>
#include <stdlib.h>
#include <ard/ard.h>
#include <dio/dio.h>

// Internal Includes
#include "nomad.h"

// Initialize Nomad
void nomad_init(struct nomad *n, uint8_t bat_level_pin, uint8_t charging_pin)
{
	// Pre-Init
	nomad_preinit(bat_level_pin, charging_pin);

	// Setup Nomad
	n->bat_level_pin = bat_level_pin;
	n->charging_pin = charging_pin;
}

// Pre-Initialize Nomad
void nomad_preinit(uint8_t bat_level_pin, uint8_t charging_pin)
{
	// Configure Charging Status Pin as Input with Pull-up
	dio_input(charging_pin);
	dio_hi(charging_pin);
}

// Is Charging
uint8_t nomad_is_charging(struct nomad *n)
{
	// Check Charging Status
	return !(dio_rd(n->charging_pin));
}

// Get Battery Level
uint8_t nomad_get_bat_level(struct nomad *n)
{
	// Read Battery Level
	return ard(n->bat_level_pin);
}

// Get Battery Voltage (millivolts)
uint16_t nomad_get_vbat(struct nomad *n)
{
	uint32_t l;

	// Get Voltage at Analog Pin (3.3V range)
	l = (((NOMAD_VREF * 128000UL) / 256) * (((uint32_t)nomad_get_bat_level(n)) + 1)) / 128;

	// Convert to Battery Voltage (4.5V range)
	return ((l / 100) * NOMAD_VCAL) / 100000;
}

// Get Battery Percentage
uint8_t nomad_get_bat_pct(struct nomad *n)
{
	uint16_t v;

	// Get Voltage
	v = nomad_get_vbat(n);

	// Determine Percentage
	if(v < NOMAD_NV0)																	{ return 0; }
	if(v >= NOMAD_NV100)																{ return 100; }
	return ((v - NOMAD_V0) * 10) / ((NOMAD_V100 - NOMAD_V0) / 10);
}
